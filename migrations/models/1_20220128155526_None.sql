-- upgrade --
CREATE TABLE IF NOT EXISTS "guild" (
    "guild" BIGSERIAL NOT NULL PRIMARY KEY,
    "logging_channel" BIGINT,
    "admin_role" BIGINT,
    "mod_role" BIGINT,
    "staff_role" BIGINT,
    "starboard_enabled" BOOL NOT NULL  DEFAULT False,
    "starboard_threshold" INT NOT NULL  DEFAULT 3,
    "starboard_channel" BIGINT
);
CREATE TABLE IF NOT EXISTS "message" (
    "message" BIGSERIAL NOT NULL PRIMARY KEY,
    "author" BIGINT NOT NULL,
    "creation_datetime" TIMESTAMPTZ NOT NULL,
    "guild_id" BIGINT NOT NULL REFERENCES "guild" ("guild") ON DELETE RESTRICT
);
CREATE TABLE IF NOT EXISTS "user" (
    "user" BIGSERIAL NOT NULL PRIMARY KEY
);
CREATE TABLE IF NOT EXISTS "member" (
    "id" BIGSERIAL NOT NULL PRIMARY KEY,
    "guild_id" BIGINT NOT NULL REFERENCES "guild" ("guild") ON DELETE RESTRICT,
    "user_id" BIGINT NOT NULL REFERENCES "user" ("user") ON DELETE RESTRICT
);
CREATE TABLE IF NOT EXISTS "tag" (
    "id" BIGSERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(16) NOT NULL,
    "creation_datetime" TIMESTAMPTZ NOT NULL,
    "content" VARCHAR(4096) NOT NULL,
    "uses" INT NOT NULL  DEFAULT 0,
    "guild_id" BIGINT NOT NULL REFERENCES "guild" ("guild") ON DELETE RESTRICT,
    "owner_id" BIGINT NOT NULL REFERENCES "user" ("user") ON DELETE RESTRICT
);
CREATE TABLE IF NOT EXISTS "aerich" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "version" VARCHAR(255) NOT NULL,
    "app" VARCHAR(20) NOT NULL,
    "content" JSONB NOT NULL
);

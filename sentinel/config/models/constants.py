#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of a private guild bot for Programmer's Palace
# Copyright (C) 2022  Forbidden-A

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import attrs

from .base import BaseConfig

__all__ = ["Constants"]


@attrs.define(frozen=True, auto_attribs=True)
class Constants(BaseConfig):
    """
    Miscellaneous assortment of constants, generally IDs.

    Attributes:
        guild        The parent guild the bot belongs to where its logs are to be sent and such.
        log_channel  The channel where bot logs should be posted.
    """

    guild: int
    logs_channel: int

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of a private guild bot for Programmer's Palace
# Copyright (C) 2022  Forbidden-A

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os

import attrs

from .base import BaseConfig

__all__ = [
    "PostgresConfig",
]


@attrs.define(frozen=True, auto_attribs=True)
class PostgresConfig(BaseConfig):
    """
    Values required to connect to The Postgres Database.
    """

    host: str
    user: str
    database: str
    port: int = 5432
    password: str = attrs.field(factory=lambda: os.environ["POSTGRES_PASSWORD"])

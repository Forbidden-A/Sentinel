#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of a private guild bot for Programmer's Palace
# Copyright (C) 2022  Forbidden-A

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import typing as t

import hikari
import lightbulb

__all__ = [
    "parse_member",
]


async def parse_member(context: lightbulb.Context, obj: hikari.SnowflakeishOr[str]) -> t.Optional[hikari.Member]:
    converter = lightbulb.MemberConverter(context)
    try:
        return await converter.convert(obj)
    except lightbulb.ConverterFailure:
        return None

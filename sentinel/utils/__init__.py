#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of a private guild bot for Programmer's Palace
# Copyright (C) 2022  Forbidden-A

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import logging
import sys

import colorlog
from hikari.internal import ux

from sentinel import Config

from .parse import parse_member

__all__ = ["enable_coloured_logging", "parse_member"]


def enable_coloured_logging(config: Config):
    """
    Configure the logger according to the values in the provided config.

    :param config: The config to get the values from.
    :returns: None
    """
    if ux.supports_color(allow_color=True, force_color=False):
        colorlog.basicConfig(
            level=config.logging.level,
            format=config.logging.coloured_log_format,
            datefmt=config.logging.date_format,
            stream=sys.stderr,
            style="{",
        )
    else:
        logging.basicConfig(
            level=config.logging.level,
            format=config.logging.normal_log_format,
            datefmt=config.logging.date_format,
            stream=sys.stderr,
            style="{",
        )

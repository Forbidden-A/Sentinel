#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of a private guild bot for Programmer's Palace
# Copyright (C) 2022  Forbidden-A

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import dotenv
import hikari

from sentinel import SentinelApp, enable_coloured_logging, get_config

dotenv.load_dotenv()
config = get_config()
# Enable coloured_logging if it's supported. I do this here instead of relying on hikari to be able to change the
# format and datefmt according to the config.
enable_coloured_logging(config)

bot = SentinelApp(token=config.discord.token, intents=hikari.Intents.ALL_UNPRIVILEGED | hikari.Intents.GUILD_MEMBERS)
bot.run()
